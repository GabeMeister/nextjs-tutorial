import Head from 'next/head';
import Layout from '../../components/Layout';

export default function FirstPost() {
  return (
    <Layout>
      <Head>
        <title>This is my first post!</title>
      </Head>
      <h1 className="title">First Post</h1>
      <style jsx>{`
        .title {
          color: #b5b5b5
        }
      `}</style>
    </Layout>
  )
}
