import Head from 'next/head';
import Layout, { siteTitle } from '../components/Layout';
import Link from 'next/link';
import useSWR from 'swr';
import axios from 'axios';

export default function Home() {
  const { data, error } = useSWR('https://ron-swanson-quotes.herokuapp.com/v2/quotes', axios.get)
  console.log('***** data *****', data);

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Link href="/posts/first-post">
        <a>First Post!</a>
      </Link>
      <section>
        {error && (
          <h1>Error Occurred while fetching Ron Swanson quote...</h1>
        )}
        {!data && (
          <h1>Fetching quote...</h1>
        )}
        {data && (
          <h1>{data.data[0]}</h1>
        )}
      </section>
    </Layout>
  )
}
